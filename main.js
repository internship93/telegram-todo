const input = document.querySelector('#input')
const tasksItem = document.querySelector('#tasks__item')
const total = document.querySelector('#total')
const btn = document.querySelector('#btn')
let count = 0;

btn.addEventListener('click', (e) =>{
	count++;
	if (input.value === '') return;
	createDeleteElement(input.value)
	input.value = '';
})

const createDeleteElement = (value) =>{
	const li = document.createElement('li');
	const div = document.createElement('div');
	const btn = document.createElement('button');
	
	div.className = 'li__value';
	div.textContent = value;

	li.className = 'li';

	btn.className = 'delBtn';
	btn.textContent = 'Delete';

	li.appendChild(div)
	li.appendChild(btn)

	btn.addEventListener('click', e =>{
		tasksItem.removeChild(li)
		count--;
	})

	total.textContent = count;
	tasksItem.appendChild(li)
}